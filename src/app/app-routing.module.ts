import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { VisualizzaCodiceFiscaleComponent } from './visualizza-codice-fiscale/visualizza-codice-fiscale.component';

const routes: Routes = [
  {path:'', component: VisualizzaCodiceFiscaleComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
