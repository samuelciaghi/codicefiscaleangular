import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-visualizza-codice-fiscale',
  templateUrl: './visualizza-codice-fiscale.component.html',
  styleUrls: ['./visualizza-codice-fiscale.component.css']
})
export class VisualizzaCodiceFiscaleComponent implements OnInit {

  risultato: any = ' - - - - - - - - - - - - - - - - ';

  mesi = {
    1: "A", 2: "B", 3: "C", 4: "D", 5: "E", 6: "H",
    7: "L", 8: "M", 9: "P", 10: "R", 11: "S", 12: "T"
  };

  datiComune: any;

  anni = [...Array(2022).keys()].map(i => i).slice(-122);
  mesiNumeri = [1, 2, 3, 4, 5, 6, 7, 8, 9, 11, 12];
  giorniMese = [...Array(30).keys()].map(g => g)

  cognome: string = 'Ciaghi'
  nome: string = 'Samuel'
  anno: number = 2002;
  mese: number = 11;
  giorno: number = 27;
  genere: string = 'M';
  comuneScelto: string = 'Rovereto';

  alfabeto = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';
  consonanti = 'bcdfghjklmnpqrstvwxyz';
  vocali = 'aeiuo';

  check: boolean = true;

  constructor() { }

  async ngOnInit(): Promise<void> {

    this.datiComune = await this.getDatiComuni()

    this.getDatiComuni()



  }

  getRisultato() {

    const primaParteCodice = new Promise<void>((resolve, reject) => {
      let giornoGenere: boolean = this.getGenere()

      this.risultato = this.getCognome(this.cognome)
        .concat(this.getNome(this.nome))
        .concat(this.getAnno(this.anno))
        .concat(this.getMese(this.mese))
        .concat(this.getGiorno(this.giorno, giornoGenere))
        .concat(this.getCodiceComune());

      resolve(this.risultato);

    });

    return primaParteCodice

  }

  getCodiceFiscale() {

    this.risultato = this.risultato.concat(this.calcolaCarattereControllo());




    let charCheck = ''

    for (let i = 0; i < this.risultato.length; i++) {

      if (this.risultato[i] != '-') {
        charCheck += this.risultato[i]
      }

    }
    if (charCheck.length < 16) { console.error("CODICE FISCALE MINORE DI 16 CARATTERI"); this.check = false };
    return this.risultato
  }

  getNome(stringa: String) {

    let nomeRidotto = '';
    let nomeVocali = '';
    
    if (stringa != null && stringa != undefined && stringa != '') {

      nomeVocali = stringa.replace(/[aeiou]/gi, '').toUpperCase();
      nomeRidotto = nomeVocali.slice(0, 3);

      if (nomeRidotto.length < 3) {
        nomeRidotto = nomeRidotto.concat(stringa.replace(/[bcdfghjklmnpqrstvwxyz]/gi, '')
          .toUpperCase()).slice(0, 3);

        if (nomeRidotto.length < 3) {
          nomeRidotto = (nomeRidotto + 'XXX').slice(0, 3);
        }
      }


    }

    else {

      nomeRidotto = "---";

    }

    return nomeRidotto
  }

  getCognome(stringa: String) {

    let cognomeRidotto = '';
    let cognomeVocali = ''

    if (stringa != null && stringa != undefined && stringa != '') {

      cognomeVocali = stringa.replace(/[aeiou]/gi, '').toUpperCase();
      cognomeRidotto = cognomeVocali.slice(0, 3);

      if (cognomeRidotto.length < 3) {
        cognomeRidotto = cognomeRidotto.concat(stringa.replace(/[bcdfghjklmnpqrstvwxyz]/gi, '')
          .toUpperCase()).slice(0, 3);

        if (cognomeRidotto.length < 3) {
          cognomeRidotto = (cognomeRidotto + 'XXX').slice(0, 3);
        }
      }
    }
    else {

      cognomeRidotto = "---";
    }

    return cognomeRidotto
  }

  getAnno(anno: number) {

    let annoRidotto: string;

    if (anno == undefined) {
      annoRidotto = '--'
      this.check = false;
    } else {
      annoRidotto = anno.toString().substr(-2);
      this.check = true;
    }



    return annoRidotto

  }

  getMese(numero: number) {

    let meseConvertito: string;

    if (numero != null && numero != undefined) {
      meseConvertito = this.mesi[numero];
      this.check = true;
    } else { this.check = false; return '-' }



    return meseConvertito

  }

  getGiorno(giorno: any, funzioneGenere: boolean) {

    if (funzioneGenere != null && funzioneGenere != undefined && giorno != null && giorno != undefined) {
      if (funzioneGenere == true) {
        giorno = ('0' + this.giorno).slice(-2)
      }
      else {
        giorno = +('0' + this.giorno).slice(-2) + 40
      }
      this.check = true;
    } else { giorno = '--'; this.check = false }
    return giorno

  }

  getGenere() {

    let letteraGenere: boolean;
    if (this.genere != null) {

      this.genere.toString().charAt(0).toUpperCase();

      if (this.genere.toUpperCase() == 'M') {
        letteraGenere = true
      }
      else if (this.genere.toUpperCase() == 'F') {
        letteraGenere = false
      }

      this.check = true;

    } else {
      letteraGenere = undefined;
      this.check = false;
    }

    return letteraGenere

  }

  async getDatiComuni() {
    return await fetch('https://comuni-ita.herokuapp.com/api/comuni')
      .then(respone => {
        return respone.json()
      })
      .then(dati => {

        return dati

      })

  }

  getCodiceComune() {
    if (this.comuneScelto != null && this.comuneScelto != undefined) {
      let comuneSelezionato = this.datiComune.filter(comune => comune.nome.toUpperCase() == this.comuneScelto.toUpperCase());
      let codiceComuneSelezionato: string;

      comuneSelezionato.forEach(comune => {
        codiceComuneSelezionato = comune.codiceCatastale
      })


      return codiceComuneSelezionato

    } else {
      console.log("API non raggiungibile e/o Comune non Trovato");
      return "----"
    }



  }

  calcolaCarattereControllo() {

    if (this.risultato != null && this.risultato != undefined && this.risultato.length == 15) {

      let codiceCompleto: number = 0;

      //Separo l'array in base alla loro posizione
      let posizionePari = [...this.risultato].filter((arr, indice) => indice % 2);
      let posizioneDispari = [...this.risultato].filter((arr, indice) => !(indice % 2));

      //Codici ricavare il valore della lettera/numero in base alla loro posizione
      let codicePari = { '0': 0, "1": 1, "2": 2, "3": 3, "4": 4, "5": 5, "6": 6, "7": 7, "8": 8, "9": 9, "A": 0, "B": 1, "C": 2, "D": 3, "E": 4, "F": 5, "G": 6, "H": 7, "I": 8, "J": 9, "K": 10, "L": 11, "M": 12, "N": 13, "O": 14, "P": 15, "Q": 16, "R": 17, "S": 18, "T": 19, "U": 20, "V": 21, "W": 22, "X": 23, "Y": 24, "Z": 25 };
      let codiceDispari = { "0": 1, "1": 0, "2": 5, "3": 7, "4": 9, "5": 13, "6": 15, "7": 17, "8": 19, "9": 21, "A": 1, "B": 0, "C": 5, "D": 7, "E": 9, "F": 13, "G": 15, "H": 17, "I": 19, "J": 21, "K": 2, "L": 4, "M": 18, "N": 20, "O": 11, "P": 3, "Q": 6, "R": 8, "S": 12, "T": 14, "U": 16, "V": 10, "W": 22, "X": 25, "Y": 24, "Z": 23 };


      //console.log('Pari: ' + posizionePari + "\n" + 'Dispari: ' + posizioneDispari);

      //Cicli per assegnare i valori di ogni carattere in base all Tabella di conversione
      for (let indice = 0; indice < posizionePari.length; indice++) {

        codiceCompleto += codicePari[posizionePari[indice]]
      }

      for (let indice = 0; indice < posizioneDispari.length; indice++) {

        codiceCompleto += codiceDispari[posizioneDispari[indice]]
      }

      codiceCompleto = codiceCompleto % 26;



      /* In base al numero risultato corrispode una lettera dell'alfabeto,
      che sarà il carattere di controllo, e quindi l'ultima lettera del Codice Fiscale */
      let letteraControllo = this.alfabeto.charAt(codiceCompleto);

      return letteraControllo
    } else {

      return "-"

    }

  }

  generaCodiceFiscale() {
    this.getRisultato().then(codiceIncompleto => this.risultato = codiceIncompleto)
      .then(b => this.getCodiceFiscale()).then(codiceCompleto => this.risultato = codiceCompleto)
  }

}
