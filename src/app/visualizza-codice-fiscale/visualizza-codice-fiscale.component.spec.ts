import { ComponentFixture, TestBed } from '@angular/core/testing';

import { VisualizzaCodiceFiscaleComponent } from './visualizza-codice-fiscale.component';

describe('VisualizzaCodiceFiscaleComponent', () => {
  let component: VisualizzaCodiceFiscaleComponent;
  let fixture: ComponentFixture<VisualizzaCodiceFiscaleComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ VisualizzaCodiceFiscaleComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(VisualizzaCodiceFiscaleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
